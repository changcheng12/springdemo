package top.changcheng.dao;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Repository;

import top.changcheng.entity.User;

@Repository
public class UserDaoImpl {

    /**
     * init.
     */
    public UserDaoImpl() {
    }

    /**
     * mocked to find user list.
     *
     * @return user list
     */
    public List<User> findUserList() {
        return Collections.singletonList(new User("pdai", 18));
    }
}
