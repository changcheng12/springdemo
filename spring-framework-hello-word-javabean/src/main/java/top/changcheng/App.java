package top.changcheng;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import top.changcheng.config.BeansConfig;
import top.changcheng.entity.User;
import top.changcheng.service.UserServiceImpl;

public class App {

    /**
     * main interfaces.
     *
     * @param args args
     */
    public static void main(String[] args) {
        // create and configure beans
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeansConfig.class);

        // retrieve configured instance
        UserServiceImpl service = context.getBean("userService", UserServiceImpl.class);

        // use configured instance
        List<User> userList = service.findUserList();

        // print info from beans
        userList.forEach(a -> System.out.println(a.getName() + "," + a.getAge()));
    }
}
